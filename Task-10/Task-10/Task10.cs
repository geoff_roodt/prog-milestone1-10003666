﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_10
{
    class Task10
    {
        /// <summary>
        /// Write a program that tells you how many leap years are there in the next 20 years (with calculation
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            var year = 2016;
            var yearCount = 0;

            var endProg = false;
            var dispLine = new string('*', 40);

            do
            {
                Console.WriteLine(dispLine);
                Console.WriteLine("The Next 20 Years:");
                Console.WriteLine("Press Enter to Continue or type 'End' to End");
                var userIn = Console.ReadLine();

                if (userIn.ToUpper() == "END")
                {
                    endProg = true;
                }
                else if (string.IsNullOrEmpty(userIn))
                {
                    for (int i = 1; i <= 20; i++)
                    {
                        var leapYear = $"{year}    -   [LEAP YEAR]";

                        // Leap years must be cleanly divisible by 4, and not by 100 and not by 400
                        var isLeapYear = year % 4 == 0 && year % 100 != 0 && year % 400 != 0;
                        if (isLeapYear)
                        {
                            yearCount++;
                            Console.WriteLine(leapYear);
                        }
                        else
                        {
                            Console.WriteLine(year.ToString());
                        }

                        year++;
                    }

                    Console.WriteLine();
                    Console.WriteLine($"There are {yearCount} leap years in the next 20 years!");
                    Console.WriteLine();
                }
                else
                {
                    Console.WriteLine();
                    Console.WriteLine("Invalid Entry!");
                }

            } while (!endProg);

            Console.WriteLine(dispLine);
            Console.WriteLine("Program Ended!");
            Console.ReadLine();
        }
    }
}
