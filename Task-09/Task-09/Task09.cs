﻿using System;

namespace Task_09
{
    class Task09
    {
        /// <summary>
        /// Program that shows which years are a leaps year for the next 20 years 
        /// </summary>
        /// <param name="args">Not Used</param>
        static void Main(string[] args)
        {
            // Leap years must be cleanly divisible by 4, and not by 100 and not by 400
            var year = 2016;
            var endProg = false;
            var dispLine = new string('*', 40);

            do
            {
                Console.WriteLine(dispLine);
                Console.WriteLine("             The Next 20 Years:");
                Console.WriteLine("Press Enter to Continue or type 'End' to End");
                var userIn = Console.ReadLine();

                if (userIn.ToUpper() == "END")
                {
                    endProg = true;
                }
                else if (string.IsNullOrEmpty(userIn))
                {
                    for (int i = 1; i <= 20; i++)
                    {
                        var leapYear = $"{year}    -   [LEAP YEAR]";
                        var isLeapYear = year % 4 == 0 && year % 100 != 0 && year % 400 != 0;

                        Console.WriteLine( isLeapYear ? leapYear : year.ToString());
                        year++;
                    }
                }

            } while (!endProg);

            Console.WriteLine(dispLine);
            Console.WriteLine("Program Ended!");
            Console.ReadLine();
        }
    }
}
