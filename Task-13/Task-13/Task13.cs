﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_13
{
    class Task13
    {
        /// <summary>
        /// This tasks requires input from user for 3 doubles and prints result to the screen
        /// Mini shop - the sum of  3 doubles by user input and add GST(15%)
        /// Output must show $ signs
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            var endProg = false;

            var itemsForSale = new Dictionary<string, double>();
            itemsForSale.Add("Orange", 3.5);
            itemsForSale.Add("Muffin", 6);
            itemsForSale.Add("Milk", 2.75);
            itemsForSale.Add("Coffee", 4.09);

            var dispLine = new string('*', 42);

            do
            {
                var itemCount = 1;
                double sum = 0;
                var itemsBought = new Dictionary<string, double>();

                Console.WriteLine();
                Console.WriteLine(dispLine);
                Console.WriteLine("Mini Shop - Choose 3 Items to buy!");
                Console.WriteLine();

                foreach (var item in itemsForSale)
                {
                    Console.WriteLine($"{item.Key} - {item.Value:C2}");
                }

                Console.WriteLine();
                Console.WriteLine(dispLine);

                while (itemCount <= 3)
                {
                    var key = string.Empty;

                    Console.WriteLine($"What item would you like to buy? [{itemCount}/3]");
                    var itemChoice = Console.ReadLine();

                    if (itemChoice.ToUpper() == "END")
                    {
                        endProg = true;
                    }
                    else if (BuyItem(itemChoice, out key))
                    {
                        var myItem = itemsForSale.FirstOrDefault(x => x.Key == key);

                        if (itemsBought.ContainsKey(myItem.Key))
                        {
                            itemsBought[myItem.Key] += myItem.Value;
                        }
                        else
                        {
                            itemsBought.Add(myItem.Key, myItem.Value);
                        }

                        itemCount++;
                    }
                    else
                    {
                        Console.WriteLine("Incorrect entry!");
                    }
                }

                Console.WriteLine();

                foreach (var item in itemsBought)
                {
                    Console.WriteLine($"You are buying: {item.Key}(s) for {item.Value:C2}");
                    sum += item.Value;
                }
                Console.WriteLine($"The total cost for your items is: {sum:C2}");

            } while (!endProg);

            Console.WriteLine();
            Console.WriteLine("Program was ended");
        }

        /// <summary>
        /// Validates user input
        /// </summary>
        /// <param name="UserInput">User's choice</param>
        /// <param name="Key">Value that is returned</param>
        /// <returns>True if valid entry, and the appropriate key</returns>
        static bool BuyItem(string UserInput, out string Key)
        {
            var isValid = false;

            switch (UserInput.ToUpper())
            {
                case "1":
                case "ORANGE":
                    isValid = true;
                    Key = "Orange";
                    break;

                case "2":
                case "MUFFIN":
                    isValid = true;
                    Key = "Muffin";
                    break;

                case "3":
                case "MILK":
                    isValid = true;
                    Key = "Milk";
                    break;

                case "4":
                case "COFFEE":
                    isValid = true;
                    Key = "Coffee";
                    break;

                default:
                    Key = string.Empty;
                    break;
            }

            return isValid;
        }
    }
}
