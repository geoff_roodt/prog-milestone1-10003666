﻿using System;
using System.Linq;

namespace Task_35
{
    class Task35
    {
        /// <summary>
        /// Write a program that asks for 5 different maths questions based on 3 numbers by user input.
        /// Display the answer on the screen.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            var endProg = false;
            var dispLine = new string('*', 60);

            do
            {
                var userIn = string.Empty;

                Console.WriteLine();
                Console.WriteLine(dispLine);
                Console.WriteLine("                 Math Calculations!");
                Console.WriteLine("Enter one of the selections below to perform it!");
                Console.WriteLine("             Or press Enter/'End' to End");
                Console.WriteLine();
                Console.WriteLine("1. Addition");
                Console.WriteLine("2. Subtraction");
                Console.WriteLine("3. Multiplication");
                Console.WriteLine("4. Division");
                Console.WriteLine("5. Power Of");
                Console.WriteLine(dispLine);

                userIn = Console.ReadLine();

                if (userIn.ToUpper() == "END" || string.IsNullOrEmpty(userIn))
                {
                    endProg = true;
                }
                else if (ValidateUserSelection(userIn))
                {
                    var numbers = new int[3];
                    var selectedOperator = DeterminUserSelection(userIn);
                    Console.WriteLine("Please Enter the 3 Numbers You Wish to Calculate:");

                    for (int i = 0; i < 3;)
                    {
                        Console.WriteLine($"Please enter your number: [{i+1}/3]");

                        var number = 0;
                        if (int.TryParse(Console.ReadLine(), out number))
                        {
                            numbers[i] = number;
                            i++;
                        }
                        else
                        {
                            Console.WriteLine("Invalid Entry!");
                        }
                    }

                    Console.WriteLine(dispLine);
                    Console.WriteLine();
                    Console.WriteLine("Your Numbers Will Now Be Calculated:");
                    Console.WriteLine($"Your Calculated Answer is: {string.Join($" {selectedOperator} ", numbers)} = {CalculateNumbers(numbers, selectedOperator)}");
                }
                else
                {
                    Console.WriteLine("Invalid Selection!");
                }

            } while (!endProg);

            Console.WriteLine();
            Console.WriteLine(dispLine);
            Console.WriteLine("Program Ended!");
            Console.ReadLine();
        }

        private static bool ValidateUserSelection(string UserInput)
        {
            return new[] {"1", "2", "3", "4", "5", "+", "-", "*", "/", "^", "ADDITION", "SUBTRACTION", "MULTIPLICATION", "DIVISION", "POWEROF"}.Any(x => x == UserInput.Replace(" ","").ToUpper());
        }

        private static string DeterminUserSelection(string UserInput)
        {
            return UserInput.Replace(" ", "").ToUpper()
                .Replace("1","+").Replace("ADDITION","+")
                .Replace("2", "-").Replace("SUBTRACTION", "-")
                .Replace("3", "*").Replace("MULTIPLICATION", "*")
                .Replace("4", "/").Replace("DIVISION", "/")
                .Replace("5", "^").Replace("POWEROF", "^");
        }

        private static double CalculateNumbers(int[] Numbers, string selectedOperation)
        {
            double sum = Numbers[0];

            for (int i = 1; i < Numbers.Length; i++)
            {
                switch (selectedOperation)
                {
                    case "+":
                        sum += Numbers[i];
                        break;

                    case "-":
                        sum -= Numbers[i];
                        break;

                    case "*":
                        sum *= Numbers[i];
                        break;

                    case "/":
                        sum /= Numbers[i];
                        break;

                    case "^":
                        sum = Math.Pow(sum, Numbers[i]);
                        break;
                }
            }

            return sum;
        }

    }
}
