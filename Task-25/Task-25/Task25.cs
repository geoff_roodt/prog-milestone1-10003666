﻿using System;

namespace Task_25
{
    class Task25
    {
        /// <summary>
        /// 1. Write a program that takes user input for a number and cannot crash
        ///     a) Check what happens when you get a word
        ///     b) Check what happens when you get a number
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            var endProg = false;
            var dispLine = new string('*', 42);

            do
            {
                var valid = false;
                Console.WriteLine();
                Console.WriteLine(dispLine);
                Console.WriteLine("Lets Handle User Input for Numbers!");
                Console.WriteLine("   Enter to Continue, 'End' to End   ");
                var temp = Console.ReadLine();
                if (temp.ToUpper() == "END")
                {
                    endProg = true;
                }
                else if (string.IsNullOrEmpty(temp))
                {
                    while (!valid)
                    {
                        var result = 0;

                        Console.WriteLine("Enter a Number!");
                        var userIn = Console.ReadLine();
                        if (int.TryParse(userIn, out result))
                        {
                            Console.WriteLine($"Successfully parsed your input! ('{userIn}' = {result})");
                            Console.WriteLine();
                            valid = true;
                        }
                        else
                        {
                            Console.WriteLine($"Unable to parse your input! - Incorrect type ('{userIn}' = ??)");
                            Console.WriteLine();
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Invalid Entry!");
                }

            } while (!endProg);

            Console.WriteLine();
            Console.WriteLine("Program Ended");
            Console.ReadLine();

        }
    }
}
