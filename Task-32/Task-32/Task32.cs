﻿using System;

namespace Task_32
{
    class Task32
    {
        /// <summary>
        /// Write a program that says what number each day of the week is - you must use an array
        /// </summary>
        /// <param name="args">Not used</param>
        static void Main(string[] args)
        {
            var endProg = false;
            var dispLine = new string('*', 44);

            do
            {
                var userIn = string.Empty;
                var daysOfWeek = new string[] {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

                Console.WriteLine();
                Console.WriteLine(dispLine);
                Console.WriteLine("        Smart Calendar!");
                Console.WriteLine("Press Enter to Start, 'End' to End");
                userIn = Console.ReadLine();
                Console.WriteLine(dispLine);

                if (userIn.ToUpper() == "END")
                {
                    endProg = true;
                }
                else if (string.IsNullOrEmpty(userIn))
                {
                    for (var i = 0; i < daysOfWeek.Length; i++)
                    {
                        Console.WriteLine($"{daysOfWeek[i]} is Day {i+1}");
                    }
                }
                else
                {
                    Console.WriteLine("Invalid Entry!");
                }

            } while (!endProg);

            Console.WriteLine();
            Console.WriteLine(dispLine);
            Console.WriteLine("Program Ended!");
            Console.ReadLine();

        }
    }
}
