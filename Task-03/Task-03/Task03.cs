﻿using System;

namespace Task_03
{
    class Task03
    {
        // Using if statements and / or switches convert KM to Miles and MIles to KM

        const double Kilometer = 0.621371;  // Declare & Initialise the constants for unit conversion
        const double Mile = 1.609344;

        static void Main(string[] args)
        {
            double convertedUnit = 0;
            double userInput = 0;         
            var userChoice = string.Empty;
            var endProgram = false;     

            do
            {
                Console.WriteLine("***************************************************");
                Console.WriteLine("Enter 'Kilometers' to convert Miles to Kilometers");
                Console.WriteLine("Or, enter 'Miles' to convert Kilometers to Miles");
                Console.WriteLine();
                Console.WriteLine("Type 'end' to finish the program");
                userChoice = Console.ReadLine().ToLower();
                Console.WriteLine("***************************************************");

                switch (userChoice)
                {
                    case "kilometers":
                        Console.WriteLine("Enter the Unit of Miles You Wish to Convert:");
                        userChoice = Console.ReadLine();

                        if (double.TryParse(userChoice, out userInput))
                        {
                            convertedUnit = userInput / Kilometer;
                            Console.WriteLine($"{userInput} Miles = {Math.Round(convertedUnit)} Kilometers");
                        }
                        else                
                        {
                            Console.WriteLine("Unable to convert to Kilometers!");
                            Console.WriteLine("*remember to enter the unit as the digit and not the word*");
                        }
                        break;

                    case "miles":
                        Console.WriteLine("Enter the unit of Kilometers You Wish to Convert:");
                        userChoice = Console.ReadLine();

                        if (double.TryParse(userChoice, out userInput))
                        {
                            convertedUnit = userInput / Mile;
                            Console.WriteLine($"{userInput} Kilometers = {Math.Round(convertedUnit)} Miles");
                        }
                        else
                        {
                            Console.WriteLine("Unable to convert to Miles!");
                            Console.WriteLine("*remember to enter the unit as the digit and not the word*");
                        }
                        break;

                    case "end":
                        endProgram = true;
                        break;

                    default:
                        Console.WriteLine("No proper calculation was selected");
                        break;
                }

            } while (!endProgram);

            Console.WriteLine("The Program Has Ended");
            Console.ReadLine();
        }
    }
}
