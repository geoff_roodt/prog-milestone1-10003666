﻿using System;
using System.Collections.Generic;

namespace Task_18
{
    class Task18
    {
        /// <summary>
        /// 1. Create a List<T> that holds people’s name and month and birthday.
        ///     a) Add 3 persons to it
        ///     b) Print them out to the screen
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            var endProg = false;
            var people = new List<Tuple<string, string, int>>();
            var dispLine = new string('*', 42);

            do
            {
                Console.WriteLine();
                Console.WriteLine(dispLine);
                Console.WriteLine("People and their Birthdate!");
                Console.WriteLine(dispLine);
                Console.WriteLine();
                Console.WriteLine("Press Enter to Continue or 'End' to End");

                var userIn = Console.ReadLine();
                if (userIn.ToUpper() == "END")
                {
                    endProg = true;
                }
                else if (string.IsNullOrEmpty(userIn))
                {

                    people.Add(Tuple.Create("Jim", "January", 31));
                    people.Add(Tuple.Create("Ellie", "February", 16));
                    people.Add(Tuple.Create("Cody", "December", 8));

                    Console.WriteLine("People are:");
                    foreach (var person in people)
                    {
                        Console.WriteLine($"{person.Item1}: {person.Item3} {person.Item2}");
                    }
                }
                else
                {
                    Console.WriteLine("Invalid Entry");
                }

            } while (!endProg);

            Console.WriteLine();
            Console.WriteLine("Program Ended");
            Console.ReadLine();
        }
    }
}
