﻿using System;

namespace Task_06
{
    class Task06
    {
        /// <summary>
        /// Forloop - calculate the sum of 5 numbers to n (n given to you by user)
        /// (User gives 5 numbers, and the sum is displayed)
        /// </summary>
        /// <param name="args">Not Used</param>
        static void Main(string[] args)
        {
            var endRound = false;
            long sum = 0;
            var count = 1;
            var displayLines = new string('*', 42);

            Console.WriteLine(displayLines);
            Console.WriteLine("Please Enter 5 Integers:");
            Console.WriteLine(displayLines);

            do
            {
                long myNumber = 0;

                Console.WriteLine($"Enter Number {count}");
                var userInput = Console.ReadLine();

                if (string.IsNullOrEmpty(userInput) || userInput.ToUpper() == "END")
                {
                    endRound = true;
                }
                else if (long.TryParse(userInput, out myNumber))
                {
                    sum += myNumber;
                    count++;
                    endRound = count > 5;
                }
                else
                {
                    Console.WriteLine("Invalid Input!");
                }

            } while (!endRound);

            Console.WriteLine(displayLines);
            Console.WriteLine($"The sum of your numbers is: {sum}");
            Console.WriteLine();

            Console.WriteLine("Program Ended");
            Console.ReadLine();
        }
    }
}
