﻿using System;

namespace Task_31
{
    class Task31
    {
        /// <summary>
        /// Write a program that checks if a number is divisible by 3 and 4 without leaving any decimal places.
        /// Inform the user of the number.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            var endProg = false;
            var dispLine = new string('*', 60);
            
            do
            {
                var userIn = string.Empty;
                var myNumber = 0;

                Console.WriteLine();
                Console.WriteLine(dispLine);
                Console.WriteLine("Is Your Number Divisible by 3 & 4 (Enter a Number):");
                userIn = Console.ReadLine();
                Console.WriteLine(dispLine);
                Console.WriteLine();

                if (userIn.ToUpper() == "END")
                {
                    endProg = true;
                }
                else if (int.TryParse(userIn, out myNumber))
                {
                    if (myNumber%3 == 0 && myNumber%4 == 0)
                    {
                        Console.WriteLine($"Number {myNumber} is cleanly divisible by 3 and 4!");
                    }
                    else
                    {
                        Console.WriteLine($"Number {myNumber} is not cleanly divisible by 3 and 4!");
                    }
                }
                else
                {
                    Console.WriteLine("Invalid Entry!");
                }

            } while (!endProg);

            Console.WriteLine();
            Console.WriteLine(dispLine);
            Console.WriteLine("Program Ended!");
            Console.ReadLine();

        }
    }
}
