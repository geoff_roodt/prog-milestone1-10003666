﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Text;
using System.Threading.Tasks;

namespace Task_28
{
    class Task28
    {
        /// <summary>
        /// 1.Create a program that takes user input of 3 words.
        ///     a) Split the string up into an array
        ///     b) Print the array to the screen with one word on each line
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            var endProg = false;
            var dispLine = new string('*', 60);

            do
            {
                var userIn = string.Empty;
                Console.WriteLine();
                Console.WriteLine(dispLine);
                Console.WriteLine("               Smart String Splitter!");
                Console.WriteLine("Enter 3 Words, seperated with spaces, to see them split!");
                Console.WriteLine("               (i.e. 'The Quick Brown') ");
                userIn = Console.ReadLine();
                Console.WriteLine();
                Console.WriteLine(dispLine);

                if (userIn.ToUpper() == "END")
                {
                    endProg = true;
                }
                else if (!string.IsNullOrEmpty(userIn) && userIn.Split().Length >= 3)
                {
                    var myWords = userIn.Split(' ');
                    foreach (var word in myWords)
                    {
                        Console.WriteLine(word);
                    }
                }
                else
                {
                    Console.WriteLine("Invalid Entry!");
                }

            } while (!endProg);

            Console.WriteLine();
            Console.WriteLine(dispLine);
            Console.WriteLine("Program Ended!");
            Console.ReadLine();
        }
    }
}
