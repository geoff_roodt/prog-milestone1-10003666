﻿using System;
using System.Linq;

namespace Task_27
{
    class Task27
    {
        /// <summary>
        /// 1. Create an array with the following values and print it in ascending order to the screen.
        ///     a) Red, Blue, Yellow, Green, Pink
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            var endProg = false;
            var dispLine = new string('*', 42);
            var colours = new string[] { "Red", "Blue", "Yellow", "Green", "Pink" };

            do
            {
                var userIn = string.Empty;

                Console.WriteLine();
                Console.WriteLine(dispLine);
                Console.WriteLine("Order a list of colours!");
                userIn = Console.ReadLine();

                if (userIn.ToUpper() == "END")
                {
                    endProg = true;
                }
                else if (string.IsNullOrEmpty(userIn))
                {
                    DisplayColours(colours);
                }
                else
                {
                    Console.WriteLine("Invalid Entry!");
                }

            } while (!endProg);

            Console.WriteLine();
            Console.WriteLine(dispLine);
            Console.WriteLine("Program Ended");
            Console.ReadLine();

        }

        static void DisplayColours(string[] colours)
        {
            var sortedColours = colours.OrderBy(s => s.Length);
            foreach (var colour in sortedColours)
            {
                Console.WriteLine(colour);
            }
        }
    }
}
