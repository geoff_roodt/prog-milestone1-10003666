﻿using System;
using System.Collections.Generic;

namespace Task_15
{
    class Task15
    {
        /// <summary>
        /// Task requires input from user and these values must be put into a List<T>
        /// 
        ///1. Add numbers in an array(create an array with 5 numbers)
        ///     a) Numbers must be added together
        ///     b) Output must be shown to the user including what their input is
        ///     
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            var endProg = false;
            double sum = 0;
            var numberList = new List<double>();

            var dispLine = new string('*', 42);

            do
            {
                var count = 1;
                Console.WriteLine();
                Console.WriteLine(dispLine);
                Console.WriteLine("Enter 5 Numbers and Receive the Sum!");
                Console.WriteLine(dispLine);
                while (count <= 3)
                {
                    double temp = 0;

                    Console.WriteLine();
                    Console.WriteLine($"Enter Number {count}: [{count}/3]");

                    var userIn = Console.ReadLine();
                    if (double.TryParse(userIn, out temp))
                    {
                        numberList.Add(temp);
                        count++;
                    }
                    else
                    {
                        Console.WriteLine("Invalid Enntry!");
                    }
                }

                foreach (var number in numberList)
                {
                    sum += number;
                }

                Console.WriteLine();
                Console.WriteLine(dispLine);
                Console.WriteLine($"The numbers you entered were: {string.Join(", ", numberList)}");
                Console.WriteLine($"The sum of these numbers are: {sum}");

            } while (!endProg);

            Console.WriteLine();
            Console.WriteLine("Program Ended");
        }
    }
}
