﻿using System;

namespace Task_01
{
    class Task01
    {
        // Get user input for name and age and show 3 different ways to print a string        
        static void Main(string[] args)
        {
            var name = string.Empty;
            int age = 0;
            var validAge = false;

            Console.WriteLine("**********************************");
            Console.WriteLine("Please Enter Your Name:");
            name = Console.ReadLine();
            Console.WriteLine("**********************************");

            do
            {
                Console.WriteLine("Please Enter Your Age as an Integer:");
                if (int.TryParse(Console.ReadLine(), out age))
                {
                    validAge = true;
                    Console.WriteLine("**********************************");
                }
                else
                {
                    Console.WriteLine("**********************************");
                    Console.WriteLine("Invalid Entry!");
                }
            } while (!validAge);

            Console.WriteLine("Greetings, Your Name is " + name);
            Console.WriteLine($"Greetings {name}, Your Age is {age}");
            Console.WriteLine("Greetings {0}, You are {1} Years Old!", name, age);

            Console.ReadLine();
        }
    }
}
