﻿using System;
using System.Globalization;
using static System.String;

namespace Task_05
{
    class Task05
    {
        /// <summary>
        /// Time from 24hr to pm / am 
        /// (show what am / pm time is when the number is between 12 and 24)
        /// </summary>
        /// <param name="args">Not Used</param>
        static void Main(string[] args)
        {
            var userInput = Join("", args);     
            var endProg = false;

            do
            {
                var newTime = new DateTime();
                Console.WriteLine(new string('*', 42));
                Console.WriteLine("Enter the 24HR Time You Wish to Convert");
                Console.WriteLine("        e.g 0800 | 1200 | 1830         ");

                userInput = IsNullOrEmpty(userInput) ? Console.ReadLine() : userInput;

                if (IsNullOrEmpty(userInput) || userInput.ToUpper() == "END")
                {
                    endProg = true;
                }
                else if (!DateTime.TryParseExact(userInput, "HHmm", new NumberFormatInfo(), DateTimeStyles.None,out newTime))
                {
                    Console.WriteLine("Invalid Entry!");
                }
                else
                {
                    Console.WriteLine($"Input: {userInput}, Output: {newTime.ToString("hh:mm tt")}");
                }

                userInput = Empty;
            }
            while (!endProg);

            Console.WriteLine("The Program Was Cancelled");
            Console.ReadLine();
        }
    }
}
