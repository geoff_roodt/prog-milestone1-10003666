﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_02
{
    class Task02
    {
        // Get user input for the month and day they are born on and display it in a sentence
        static void Main(string[] args)
        {
            var month = string.Empty;
            int day = 0;           
            var validDate = false;

            Console.WriteLine("******************************************************");
            Console.WriteLine("Please Enter the Name of the Month You Were Born In:");
            month = Console.ReadLine();
            Console.WriteLine("******************************************************");

            do
            {
                Console.WriteLine("Please Enter the Date (as an integer) That You Were Born On:");
                if (int.TryParse(Console.ReadLine(), out day))
                {
                    validDate = true;
                    Console.WriteLine("******************************************************");
                }
                else
                {
                    Console.WriteLine("******************************************************");
                    Console.WriteLine("Invalid Entry!");
                }
            }
            while (!validDate);

            Console.WriteLine($"You Were Born on {month} the {day}!");
            Console.ReadLine();
        }
    }
}
