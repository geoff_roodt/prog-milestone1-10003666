﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace Task_07
{
    class Task07
    {
        /// <summary>
        /// Show times table from 1 x to 12 x for a given number - each answer on a new line
        /// </summary>
        /// <param name="args">Not Used</param>
        static void Main(string[] args)
        {
            const int TIMES_TABLE = 12;

            var userInput = string.Empty;
            var displayBorder  = new string('*', 42);
            var endProg = false;

            do
            {
                Console.WriteLine(displayBorder);
                Console.WriteLine("Enter an Integer for Timestable");
                userInput = Console.ReadLine();

                if (string.IsNullOrEmpty(userInput) || userInput.ToUpper() == "END")
                {
                    endProg = true;
                }
                else
                {
                    if (ValidateNumber(userInput))
                    {
                        for (var i = 1; i <= TIMES_TABLE; i++)
                        {
                            // var bNum = new BigInteger();
                            // BigInteger.TryParse(userInput, out bNum);
                            // var bigint = BigInteger.Multiply(bNum, new BigInteger(i));
                            Console.WriteLine($"{i} x {userInput} = {FormatNumber(CalculateSum(userInput,i.ToString()))}");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Invalid Entry!");
                    }

                    
                    Console.WriteLine();
                }


            } while (!endProg);

            Console.WriteLine("Program Ended");
            Console.ReadLine();
        }

        #region Calculation

        /// <summary>
        /// Calculates the user input with the current position in timestable
        /// </summary>
        /// <param name="p1">The User Input</param>
        /// <param name="p2">Position of Timestable</param>
        /// <returns>The calculation (multiplication)</returns>
        private static string CalculateSum(string p1, string p2)
        {
            var sum = "";
            var isNegative = p1.Contains("-");
            var isDecimal = p1.Contains(".");

            p1 = isNegative ? string.Join("", p1.Skip(1)) : p1;

            if (isDecimal)
            {
                var p1Split = p1.Split('.');
                var wholeNumbers = p1Split.First();
                var decimalNumbers = p1Split.Last();

                var wholeSum = MultiplyNumbers(wholeNumbers, p2);
                var decimalSum = MultiplyNumbers(decimalNumbers, p2);

                var decimalLeft = "";

                // Check if there was a change in decimal precision
                if(decimalSum.Length - decimalNumbers.Length != 0)
                {
                    // Get the whole numbers from the decimal sum
                    decimalLeft = string.Join("", decimalSum.Reverse().Skip(decimalNumbers.Length).Reverse());
                }
                
                // Add the whole numbers from the decimal sum to the whole sum
                var wholeLeft = AddNumbers(wholeSum, decimalLeft);
                // Add the whole numbers to the final sum
                sum = string.IsNullOrWhiteSpace(wholeLeft) ? "0" : wholeLeft;
                // Finally add the decimal numbers, restoring the original precision
                sum += $".{string.Join("", decimalSum.Skip(decimalSum.Length - decimalNumbers.Length)).TrimEnd('0')}";
            }
            else
            {
                sum = MultiplyNumbers(p1, p2);
            }
            
            return (isNegative ? "-" : "") + sum;
        }

        /// <summary>
        /// adds two strings together 
        /// that are larger than
        /// the builtin value types
        /// </summary>
        /// <param name="p1">longest number</param>
        /// <param name="p2">shortest number</param>
        /// <returns>The sum of p1 and p2 and if there was an overflow</returns>
        private static string AddNumbers(string p1, string p2)
        {
            var sum = "";
            // Order numbers from largest to smallest
            var numbers = new List<string> {p1.TrimStart('0'), p2.TrimStart('0') };
            numbers.Sort((a, b) => b.Length - a.Length);

            // Reversing numbers
            var p1Number = numbers.First().Reverse().Select(x => int.Parse(x.ToString())).ToList();
            var p2Number = numbers.Last().Reverse().Select(x => int.Parse(x.ToString())).ToList();
            var overflow = 0;

            for (var i = 0; i < p1Number.Count; i++)
            {
                // Add the numbers together
                var nSum = p1Number[i] + (p2Number.Count > i ? p2Number[i] : 0) + overflow;
                // Calculate the overflow (i.e. when 9+1 = 10, '1' is overflow)
                overflow = nSum >= 10 ? nSum / 10 : 0;
                sum = nSum%10 + sum;
            }
            sum = (overflow != 0 ? overflow.ToString() : "") + sum;

            return sum;
        }

        /// <summary>
        /// multiplys two strings together
        /// that are larger than
        /// the builtin value types
        /// </summary>
        /// <param name="p1">longest number</param>
        /// <param name="p2">shortest number</param>
        /// <returns>The multiplication of p1 and p2 and if there was an overflow</returns>
        private static string MultiplyNumbers(string p1, string p2)
        {
            var sum = "";
            var numbers = new List<string> { p1.TrimStart('0'), p2.TrimStart('0') };
            numbers.Sort((a, b) => b.Length - a.Length);

            var p1Number = numbers.First().Select(x => int.Parse(x.ToString())).ToList();
            var p2Number = numbers.Last().Select(x => int.Parse(x.ToString())).ToList();
            var overflow = 0;

            for (var i = p2Number.Count - 1; i >= 0; i--)
            {
                // We will need trailing 0's for the calculations to come
                var numberSum = new string('0', p2Number.Count - 1 - i);
                for (var j = p1Number.Count - 1; j >= 0; j--)
                {
                    var nSum = p1Number[j] * (p2Number.Count > i ? p2Number[i] : 0) + overflow;
                    overflow = nSum >= 10 ? nSum / 10 : 0;
                    numberSum = nSum % 10 + numberSum;
                }
                // Add the current sum to the whole sum
                sum = AddNumbers((overflow != 0 ? overflow.ToString() : "") + numberSum, sum);
                overflow = 0;
            }

            return sum;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Display the final sum with commas
        /// </summary>
        /// <param name="number">Input number</param>
        /// <returns>Formatted Input number</returns>
        private static string FormatNumber(string number)
        {
            var numberSplit = number.Split('.');
            var wholeNumbers = numberSplit.First().Reverse();
            var decimalNumbers = numberSplit.Last();
            
            // Group numbers by 3
            List<string> a = wholeNumbers.Select((c, i) => new { Char = c, Index = i }).GroupBy(o => o.Index / 3).Select(g => new string(g.Select(o => o.Char).ToArray())).ToList();

            var returnString = string.Join("", string.Join(",", a).Reverse());
            if (returnString.Contains('-'))
            {
                returnString = returnString.Replace("-,", "-");
            }

            return returnString + (numberSplit.Length > 1 ? "." +  decimalNumbers : "");
        }

        /// <summary>
        /// Validates input string as a number
        /// </summary>
        /// <param name="number">The user input</param>
        /// <returns>Bool</returns>
        private static bool ValidateNumber(string number)
        {
            if (number.Count(x => x == '-') == 0 || number.Count(x => x == '-') == 1 && number.IndexOf("-") == 0)
            {
                if (number.Count(x => x == '.') <= 1)
                {
                    if (number.Replace("-", "").Replace(".", "").All(char.IsDigit))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        #endregion
    }
}
