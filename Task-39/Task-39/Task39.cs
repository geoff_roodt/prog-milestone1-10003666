﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task_39
{
    class Task39
    {
        /// <summary>
        /// Assuming that the 1st of the month is on a Monday (like this month)
        /// Write a program that tells you how many mondays are in a month.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            var endProg = false;
            var dispLine = new string('*', 60);

            do
            {
                var userIn = string.Empty;

                Console.WriteLine();
                Console.WriteLine(dispLine);
                Console.WriteLine("                 How Many Mondays Are There?");
                Console.WriteLine("Enter a month to find out! - Assuming each month starts on a Monday");
                userIn = Console.ReadLine();
                Console.WriteLine(dispLine);

                if (userIn.ToUpper() == "END" || string.IsNullOrEmpty(userIn))
                {
                    endProg = true;
                }
                else if (ValidateMonth(userIn))
                {
                    Console.WriteLine($"There are {DetermineDays(userIn)} Mondays in {userIn} ");
                }
                else
                {
                    Console.WriteLine("Invalid Entry!");
                }

            } while (!endProg);

            Console.WriteLine();
            Console.WriteLine(dispLine);
            Console.WriteLine("Program Ended");
        }

        private static bool ValidateMonth(string UserIn)
        {
            return new[]{
                        "JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER",
                        "NOVEMBER", "DECEMBER"
                        }.Any(x => x == UserIn.ToUpper());
        }

        private static int DetermineDays(string Month)
        {
            var sum = 0;
            var months = new Dictionary<string, int>();
            months.Add("JANUARY",31);
            months.Add("FEBRUARY",29);
            months.Add("MARCH",31);
            months.Add("APRIL",30);
            months.Add("MAY",31);
            months.Add("JUNE",30);
            months.Add("JULY",31);
            months.Add("AUGUST",31);
            months.Add("SEPTEMBER",30);
            months.Add("OCTOBER",31);
            months.Add("NOVEMBER",30);
            months.Add("DECEMBER",31);

            var myDays = months.First(x => x.Key == Month.ToUpper()).Value;
            // Will always return 5
            sum = myDays/7 + 1;

            return sum;
        }

    }
}
