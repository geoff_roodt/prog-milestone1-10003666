﻿using System;
using System.Linq;

namespace Task_33
{
    class Task33
    {
        /// <summary>
        /// Write a program that will tell you how many tutorial groups need to be made based on a number given by the user.
        /// Each tutorial group has a maximum of 28 students.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            var endProg = false;
            var dispLine = new string('*', 70);

            do
            {
                var userIn = string.Empty;
                var myNumber = 0;

                Console.WriteLine();
                Console.WriteLine(dispLine);
                Console.WriteLine("                     Tutorial Division!");
                Console.WriteLine("Enter a Number to See How Many Tutorial Groups Will Fit Into It!");
                Console.WriteLine("                 Press Enter, or 'End' to End");
                userIn = Console.ReadLine();
                Console.WriteLine(dispLine);

                if (userIn.ToUpper() == "END" || string.IsNullOrEmpty(userIn))
                {
                    endProg = true;
                }
                else if (int.TryParse(userIn, out myNumber))
                {
                    var numParts = Decimal.Divide(myNumber, 28).ToString().Split('.');
                    if (numParts.Length == 1)
                    {
                        Console.WriteLine($"The Number of Groups You Will need is: {numParts.First()}");
                    }
                    else
                    {
                        Console.WriteLine($"The Number of Groups You Will need is: {int.Parse(numParts.First()) + 1}");
                    }
                }
                else
                {
                    Console.WriteLine("Invalid Entry!");
                }

            } while (!endProg);

            Console.WriteLine();
            Console.WriteLine(dispLine);
            Console.WriteLine("Program Ended");
            Console.ReadLine();

        }
    }
}
